from django.urls import path
from .views import index, redirect_to_full_url

urlpatterns = [
    path('', index, name='index'),
    path('<str:short_url>/', redirect_to_full_url, name='redirect_to_full_url'),
]