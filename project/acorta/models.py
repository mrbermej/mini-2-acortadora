from django.db import models

# Create your models here.
class Url(models.Model):
    clave = models.CharField(max_length=100) #esta es la url que introduciremos
    valor = models.TextField() #esta será el nombre que elijamos para acortar la url

class Contador_urls_acortadas(models.Model):
    clave = models.CharField(max_length=100) #esta será el numero de la url acortada