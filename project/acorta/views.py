from django.http import HttpResponse
from .models import Url, Contador_urls_acortadas
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect


@csrf_exempt
def index(request):
    if request.method == "POST" and "contenido" in request.POST:
        url = request.POST["contenido"]
        if len(url) > 0:
            url_completa = completar(url)
            short_url = request.POST["short_url"]
            # Verificar si se proporcionó un número para acortar
            if len(short_url) > 0:
                # Si se proporcionó un número, verificar si ya se está utilizando
                short_url = contador_urls_acortadas(short_url)
            else:
                # Si no se proporcionó ningún número, obtener el siguiente número disponible
                short_url = contador_urls_acortadas('')
            # Guardar la URL en la base de datos
            contents = Url(clave=url_completa, valor=short_url)
            contents.save()
    urls = lista_urls()
    return render(request, "acorta/form.html", {"urls": urls})

#crear función que recorra la lista de urls acortadas y vea si hay alguna que coincidad con el numero de la url que se va a acortar
# (si tengo una url acortada con el numero 1, no debería poder crear una nueva url acortada con el numero 1)
# si no que debería crear una nueva url acortada con el numero 2

def completar(url):
    if url.startswith("https://") or url.startswith("http://"):
        return url
    else:
        return "https://" + url

def contador_urls_acortadas(url):
    if not url:  # Si no se proporcionó un número en short_url, buscar el siguiente número disponible
        contador = 1
        while Url.objects.filter(valor=str(contador)).exists():
            contador += 1
    else:  # Si se proporcionó un número en short_url, verificar si está siendo utilizado
        contador = int(url)
        while Url.objects.filter(valor=str(contador)).exists():
            contador += 1
    return str(contador)

def lista_urls():
    lista = {}
    for x in Url.objects.all():
        lista[x.clave]= x.valor
    return lista

def redirect_to_full_url(request, short_url):
    try:
        url = Url.objects.get(valor=short_url)
        return redirect(url.clave)
    except Url.DoesNotExist:
        # Manejar caso de URL acortada no encontrada
        return redirect('index')  # Redirigir a la página principal o a otra vista

